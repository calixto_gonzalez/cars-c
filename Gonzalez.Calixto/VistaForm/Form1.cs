﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VistaForm
{
    public partial class FrmPickUp : Form
    {
        public FrmPickUp()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }

        private void modelo_TextChanged(object sender, EventArgs e)
        {

        }

        private void patente_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCrear_onClick(object sender, EventArgs e)
        {
            if (this.patente_textBox != null && this.modelo_textBox != null)
            {
                Entidades.PickUp pickUp = new Entidades.PickUp(this.patente_textBox.Text, this.modelo_textBox.Text);
                if (pickUp.Patente == null || pickUp.Modelo == null)
                {
                    MessageBox.Show("Algun dato ingresado no es correcto");
                } else
                {
                    MessageBox.Show(pickUp.ConsultarDatos());
                    this.Close();
                }
            }


        }
       
           
    }
}
