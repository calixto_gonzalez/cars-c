﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class Estacionamiento
    {
        private int espacioDisponible;
        private List<Vehículo> vehículos;
        private string nombre;

        public Estacionamiento()
        {
            vehículos = new List<Vehículo>();
        }

        public Estacionamiento(string nombre, int espacioDisponible) : this()
        {
            this.nombre = nombre;
            this.espacioDisponible = espacioDisponible;

        }

    public static explicit operator string(Estacionamiento e)
        {

            StringBuilder toString = new StringBuilder();
            toString.AppendLine(e.Nombre);
            foreach (Vehículo vehículo in e.vehículos)
            {
                toString.AppendLine(vehículo.ConsultarDatos());
            }

            return toString.ToString();
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public int EspacioDisponible
        {
            get { return espacioDisponible; }
            set { espacioDisponible = value; }
        }

        public static bool operator == (Estacionamiento e, Vehículo v)
        {

            return e.vehículos.Contains(v);
        }

        public static bool operator != (Estacionamiento e, Vehículo v)
        {
            return !(e == v);
        }

        public static Estacionamiento operator +(Estacionamiento e, Vehículo v)
        {
            if (e == v || v.Patente == null || e.espacioDisponible < e.vehículos.ToArray().Length)
            {
                return e;
            } else {
                e.vehículos.Add(v);
                return e;
            }
        }

        public static string operator -(Estacionamiento e, Vehículo v)
        {
            if (Object.ReferenceEquals(v, null) || !e.vehículos.Contains(v))
            {
                return "El Vehiculo no es parte del estacionamiento";

            } else
            {
                e.vehículos.Remove(v);
                return v.ImprimirTciket();
            }
        }

    }
}
