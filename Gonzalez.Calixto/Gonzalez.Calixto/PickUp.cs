using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
  public class PickUp : Vehículo
  {
    private string modelo;
    static int valorHora;

    static PickUp()
    {
      valorHora = 70;
    }


    public PickUp(string patente, string modelo) : base(patente)
    {
      this.modelo = modelo;
      this.Patente = patente;
    }

    public PickUp(string patente, string modelo, int valorHora) : this(patente, modelo)
    {
      PickUp.valorHora = valorHora;

    }

    

    public int ValorHora
    {
      get { return valorHora; }
      set { valorHora = value; }
    }


    public string Modelo
    {
      get { return modelo; }
      set { modelo = value; }
    }

        public override string ConsultarDatos()
        {
            return new StringBuilder().AppendFormat("PickUp\nPatente {0} modelo {1} Ingreso {2} Valor Hora {3} \n", Patente, modelo, Ingreso, valorHora).ToString();
        }


        public override bool Equals(object obj)
    {
      Vehículo pickUp = (Vehículo) obj;
      return pickUp.Patente == this.Patente;
    }

        public override string ImprimirTciket()
        {
            return base.ImprimirTciket()+"Costo de estadía: "+ ((DateTime.Now - Ingreso).Hours * valorHora);
        }
    }
}
