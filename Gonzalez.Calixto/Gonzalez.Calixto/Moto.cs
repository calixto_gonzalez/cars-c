﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class Moto : Vehículo
    {
        private static int valorHora;
        private int cilindrada;
        private short ruedas;

    

        static Moto()
        {
            valorHora = 30;
        }

        public Moto(string patente, int cilindrada) : base(patente)
        {
            this.Patente = patente;
            this.cilindrada = cilindrada;
        }

        public Moto(string patente, int cilindrada, short ruedas) : this(patente, cilindrada)
        {
            Ruedas = ruedas;
        }

        public Moto(string patente, int cilindrada, short ruedas, int valorHora) : this(patente,cilindrada,ruedas)
        {
            Moto.valorHora = valorHora;
        }
        public int Cilindrada
        {
            get { return cilindrada; }
            set { cilindrada = value; }
        }

        public short Ruedas
        {
            get { return ruedas; }
            set { ruedas = value; }
        }


        public override string ConsultarDatos()
        {
            return new StringBuilder().AppendFormat("Moto  Patente {0} Cilindrada {1} Ingreso {2} Valor Hora {3} \n", Patente, Cilindrada, Ingreso, valorHora).ToString();
        }

        public override string ImprimirTciket()
        {
            return base.ImprimirTciket() + "Costo de estadía: " + ((DateTime.Now - Ingreso).Hours * valorHora);
        }
    }
}
