using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class Automovil : Vehículo
    {


    private static int valorHora;
    private ConsoleColor color;

    static Automovil()
    {
      valorHora = 50;
    }

    public Automovil(string patente, ConsoleColor consoleColor) : base(patente)
    {
      this.Patente = patente;
      this.Color = consoleColor;
    }

    public Automovil(string patente, int valorHora, ConsoleColor color) : this(patente, color)
    {
      Automovil.valorHora = valorHora;
    }

    public int ValorHora
    {
      get { return valorHora; }
      set { valorHora = value; }
    }



    public ConsoleColor Color
    {
      get { return color; }
      set { color = value; }
    }

        public override string ConsultarDatos()
        {
            return new StringBuilder().AppendFormat("Automovil  Patente {0} Color {1} Ingreso {2} Valor Hora {3} \n", Patente, color, Ingreso, valorHora).ToString();
        }

        public override bool Equals(object obj)
    {
      Vehículo automovil = obj as Vehículo;
      return automovil.Patente == this.Patente;
    }

    public override string ImprimirTciket()
    {
            return base.ImprimirTciket() + "Costo de estadía: " + ((DateTime.Now - Ingreso).Hours * valorHora);
    }

    }
}
