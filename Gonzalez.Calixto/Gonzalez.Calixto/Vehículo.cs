using System;
using System.Text;

namespace Entidades
{

  public abstract class Vehículo
  {
    private DateTime ingreso;
    private string patente;

    public Vehículo(string patente)
    {
      Patente = patente;
      ingreso = DateTime.Now.AddHours(-3);
    }

    public string Patente
    {
      get { return patente; }
      set {
        if (value.Length == 6) {
          patente = value;
        }
      }
    }

    public DateTime Ingreso
    {
      get { return ingreso; }
      set { ingreso = value; }
    }

    public abstract string ConsultarDatos();

    public override string ToString()
    {
      return String.Format("Patente {0}", patente);
    }

    public virtual string ImprimirTciket()
    {
      return new StringBuilder().Append(this).AppendLine(ingreso.ToString("dd MMMM yyyy hh:mm:ss")).ToString();
    }

    public static bool operator ==(Vehículo vehículo1, Vehículo vehículo2)
    {
      if (object.ReferenceEquals(vehículo1, null) || object.ReferenceEquals(vehículo2, null))
      {
        return false;
      } else
      {
        return vehículo1.Patente == vehículo2.Patente;
      }


    }

    public static bool operator !=(Vehículo vehículo1, Vehículo vehículo2) {
      return !(vehículo2 == vehículo1);
    }
  }
}
